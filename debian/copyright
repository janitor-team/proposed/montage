Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: montage
Source: http://montage.ipac.caltech.edu/docs/download.html
Files-Excluded: lib/src/wcstools-* lib/src/wcssubs* lib/src/cfitsio-*
 lib/src/jpeg-8b lib/src/freetype-* lib/src/bzip2* data/fonts Windows

Files: * lib/src/montage_wcs/str2dcpp.c lib/src/montage_wcs/str2dsun.c
Copyright: 2001-2017 California Institute of Technology, Pasadena, California.
 2002 Michael Ringgaard
 1988-1993 The Regents of the University of California.
 1994 Sun Microsystems, Inc.
License: BSD-3-Clause
 Based on Cooperative Agreement Number NCC5-626 between NASA and the
 California Institute of Technology. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions of this
 BSD 3-clause license are met:
 .
  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
 .
  3. Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 This software was developed at the Infrared Processing and Analysis Center
 (IPAC) and the Jet Propulsion Laboratory (JPL) by Bruce Berriman, John Good,
 Joseph Jacob, Daniel S. Katz, and Anastasia Laity.

Files: lib/src/lodepng*
Copyright: 2005-2014 Lode Vandevenne
License: libpng
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.
 .
  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.
 .
  3. This notice may not be removed or altered from any source
  distribution.

Files: util/Search/rtree/* util/MovingTarget/rtree/* lib/src/montage_wcs/*
Copyright: 2001 GRASS Development Team
 1996-2009 Smithsonian Astrophysical Observatory, Cambridge, MA, USA
 1995-2005 Mark Calabretta
 1986 association of universities for research in astronomy inc.
 1991-1999 John B. Roll jr.
License: GPL-2+

Files: ancillary/HPXcvt.c
Copyright: 1995-2016, Mark Calabretta
License: GPL-3+
 WCSLIB is free software: you can redistribute it and/or modify it under the
 terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 WCSLIB is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

Files: python/MontagePy/MontagePy/FreeSans.ttf
Copyright: 2002-2012, GNU Freefont contributors
License: GPL-3+ with Special Font Exception
 GNU FreeFont is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3 of the License, or (at your option)
 any later version.
 .
 The fonts are distributed in the hope that they will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3+ with Special Font Exception
 GNU FreeFont is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3 of the License, or (at your option)
 any later version.
 .
 The fonts are distributed in the hope that they will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 * Regarding the use of built font files.
 .
 As a special exception, if you create a document which uses this font, and
 embed this font or unaltered portions of this font into the document, this
 font does not by itself cause the resulting document to be covered by the
 GNU General Public License. This exception does not however invalidate any
 other reasons why the document might be covered by the GNU General Public
 License. If you modify this font, you may extend this exception to your
 version of the font, but you are not obligated to do so.  If you do not
 wish to do so, delete this exception statement from your version.
 .
 * Clarification of exception, regarding intended use of embedded fonts
 .
 The exception to the GNU General Public License for the GNU FreeFont
 software exempts a user that embeds part or all of the GNU FreeFont software
 in a document, as well as subsequent users and distributors of the document,
 from complying with the GNU GPL under some circumstances.
 .
 Specifically, this exemption applies when the embedder has not modified
 the GNU FreeFont software before embedding it. This is true even though
 portions of the software become part of the document.
 .
 Therefore users of fonts released with this exception need not concern
 themselves with the license terms to: 
 .
    Create a printable file using the fonts.
    - Publish such a file.
    - Redistribute such a file.
    - Publicly display such a file.
    - Print such a file or display it on the screen.
 .
 However anyone distributing the font software as software separately from
 the document, even if the way the font software was obtained was to extract
 it from a document, would have to comply with the GNU GPL.
 .
 A modified version of the GNU FreeFont software must comply with the GNU GPL.
 It may optionally carry the same exception as the GNU FreeFont software
 itself, and we recommend releasing modified versions that way. If it does,
 then users will likewise be permitted to embed the modified version in
 documents and be exempt from the GNU GPL requirements on those documents.

Files: debian/*
Copyright: 2014 Gijs Molenaar <gijs@pythonic.nl>
 2015 Ole Streicher <olebole@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
