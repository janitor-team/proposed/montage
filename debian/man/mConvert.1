.TH MCONVERT 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mConvert \- Convert FITS data to a different data type (ie, integer to floating-point)

.SH SYNOPSIS
mConvert [\-d \fIlevel\fP] [\-s \fIstatusfile\fP] [\-b \fIbitpix\fP] [\-min \fIminval\fP] [\-max \fImaxval\fP] [\-blank \fIblankval\fP] in.fits out.fits

.SH DESCRIPTION
\fBmConvert\fP changes the datatype of an image.  When converting to floating point, no additional information is needed.  However, when converting from higher precision (e.g. 64\-bit floating point) to lower (e.g. 16\-bit integer), scaling information is necessary.  This can be given explicitly by the user or guessed by the program.

.SH OPTIONS
.TP
\-d \fIlevel\fP
Turns on debugging to the specified level (1\-3).
.TP
\-s \fIstatusfile\fP
\fBmBgModel\fP output and errors are written to \fIstatusfile\fP instead of to stdout.
.TP
\-b \fIbitpix\fP
BITPIX value for the output FITS file (default is \-64).  Possible values are:

8 (character or unsigned binary integer)
16 (16\-bit integer)
32 (32\-bit integer)
\-32 (single precision floating point)
\-64 (double precision floating point).
.TP
\-min \fIminval\fP
Pixel data value in the input image which should be treated as a minimum (value of 0) in the output image when converting from floating point to integer.  (default for BITPIX 8: 0; BITPIX 16: \-32767; BITPIX 32: \-2147483647
.TP
\-max \fImaxval\fP
Pixel data value in the input image which should be treated as a maximum (value of 255 or 32768) in the output image when converting from floating point to integer. (Default for BITPIX 8: 255; BITPIX 16: 32768; BITPIX 32: 2147483648)
.TP
\-blank \fIblankval\fP
If converting down to an integer scale: value to be used in the output image to represent blank pixels (NaN) from the input image. Default value is \fIminval\fP.

.SH ARGUMENTS
.TP
in.fits
Input image filename
.TP
out.fits
Output image filename.

.SH RESULT
Output image with the datatype as specified by the user (BITPIX).

.SH MESSAGES
.TP
OK
[struct stat="OK"]
.TP
ERROR
No status file name given
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
No debug level given
.TP
ERROR
Debug level string is invalid: '\fIdebug-level\fP'
.TP
ERROR
Debug level value cannot be negative
.TP
ERROR
No bitpix value given
.TP
ERROR
Bitpix string is invalid: '\fIbitpix\fP'
.TP
ERROR
Bitpix must be one of (8, 16, 32, \-32, \-64)
.TP
ERROR
No range min value given
.TP
ERROR
Range min string is invalid: '\fImin\fP'
.TP
ERROR
No range max value given
.TP
ERROR
Range max string is invalid: '\fImax\fP'
.TP
ERROR
No blank value given
.TP
ERROR
Blank string is invalid: '\fIblank\fP'
.TP
ERROR
Invalid input file '\fIin.fits\fP'
.TP
ERROR
Invalid output file '\fIout.fits\fP'
.TP
ERROR
\fIgeneral error message\fP
.TP
ERROR
\fIFITS library error message\fP

.SH EXAMPLES
.PP
Converting a single-precision image down to a 16\-bit integer BITPIX, when the data is clustered between values of \-0.01 and 0.1:
.TP
$ mConvert \-b 16 \-min \-0.01 \-max 0.1 \-blank \-32767 acs.fits acs_bitpix16.fits
[struct stat="OK"]

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
