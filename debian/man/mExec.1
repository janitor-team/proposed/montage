.TH MEXEC 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mExec \- Create a 2MASS, SDSS or DSS mosaic based on a size and location

.SH SYNOPSIS
mExec [\-q] [\-r \fIrawdir\fP] [\-n \fIntilex\fP] [\-m \fIntiley\fP] [\-l] [\-k] [\-c] [\-o \fIoutput.fits\fP] [\-d \fIlevel\fP] [\-f \fIregion.hdr\fP | \-h \fIheader\fP] survey band [\fIworkspace-dir\fP]

.SH DESCRIPTION
The \fBmExec\fP module is a mosaicking executive for 2MASS, SDSS, and DSS data.  It includes remote data and metadata access.  Alternatively, users can mosaic a set of data already on disk.

.SH OPTIONS
.TP
\-q
Use mProjectQL for reprojections.  Much faster but not quite flux-conserving.
.TP
\-r \fIrawdir\fP
Provide path to directory containing original ("raw") data which will be reprojected and mosaicked.  Not necessary if using \fBmExec\fP to retrieve remote data from the 2MASS, SDSS or DSS surveys.
.TP
\-n \fIntilex\fP
Number of output tiles to create along the X-axis \- default is 1 for a single mosaicked image.
.TP
\-m \fIntiley\fP
Number of output tiles to create along the Y-axis \- default is equal to \fIntilex\fP.
.TP
\-l
"Level-only" flag (see \fBmBgModel\fP)
.TP
\-k
If retrieving data from a remote archive, the "keep" flag will leave the original data products on disk after generating a mosaic.  Without this flag, raw data will be deleted (unless it was provided by the user with the "\-r" flag).
.TP
\-c
Remove all temporary files and intermediate data products.  Note: if not using the '\-o' flag to specify an output file, this will also remove mosaic.fits.
.TP
\-o \fIoutput.fits\fP
Provide your own filename for the output mosaic.  Default filename is "mosaic.fits."
.TP
\-d \fIlevel\fP
Print out additional debugging information (levels 1\-4)

.SH ARGUMENTS
.PP
\fBmExec\fP must be provided with FITS header information.  This can be in the form of a header template ("\-f" flag), or the user can pass header text directly to the program ("\-h" flag).
.TP
\-f \fIregion.hdr\fP
Path to header template used to create mosaic.
.TP

\-h \fIheader\fP

Provide header template as text input rather than point to a file; see sample shell script that makes use of this option.
.TP
survey band
If not mosaicking user-provided data ("\-r" flag), must select one of the following combinations of survey and band:

2MASS [j, h, k]
SDSS [u, g, r, i, z]
DSS [DSS1, DSS1R, DSS1B, DSS2, DSS2B, DSS2R, DSS2IR]

.TP
workspace-dir
Directory where intermediate files will be created.  If no workspace is given, a unique local subdirectory will be created (e.g.; ./MOSAIC_AAAaa17v)

.SH RESULT
\fB[struct stat="OK", workspace="\fIworkspace-dir\fP"]\fP
.PP
The output mosaic(s), and any intermediate files (if the "\-c" flag was not used) will be inside \fIworkspace-dir\fP.

.SH MESSAGES
.TP
OK
[struct stat="OK", workspace="\fIworkdir\fP"]
.TP
ERROR
Must have either header file (\-f) or header text (\-h)
.TP
ERROR
Can't open original header template file: [\fIregion.hdr\fP]
.TP
ERROR
Can't open workspace header template file: [\fIregion.hdr\fP]
.TP
ERROR
Can't create proper subdirectories in workspace (may already exist)
.TP
ERROR
Can't open header template file
.TP
ERROR
Can't open expanded header file: [big_region.hdr]
.TP
ERROR
\fIsurvey\fP has no data covering expanded area
.TP
ERROR
\fIsurvey\fP has no data covering this area
.TP
ERROR
No data was available for the region specified at this time
.TP
ERROR
Bad WCS in input image
.TP
ERROR
Can't open mosaic file: [mosaic.fits]
.TP
ERROR
Can't open save file: [image.fits]
.TP
ERROR
Output wcsinit() failed
.TP
ERROR
\fIError from another Montage module\fP

.SH EXAMPLES
.PP
To create a small mosaic of the area around m31, located at coordinates "10.68469 41.26904":
.TP
$ mHdr m31 0.3 m31.hdr
[struct stat="OK", count="16"]
.TP
$ mExec \-o m31.fits \-f m31.hdr 2MASS J tempdir
[struct stat="OK", workspace="/path/to/tempdir"]
.PP
To mosaic your own collection of data, contained in the directory "raw":
.TP
$ mImgtbl raw raw.tbl
[struct stat="OK", count=16, badfits=0]
.TP
$ mMakeHdr raw.tbl raw.hdr

[struct stat="OK", count=16, clon=0.118289, clat=0.118288, lonsize=0.236660, latsize=0.236660, posang=359.999756, lon1=0.236618, lat1=\-0.000042, lon2=359.999959, lat2=\-0.000041, lon3=359.999959, lat3=0.236618, lon4=0.236620, lat4=0.236617]
.TP
$ mExec \-o my_mosaic.fits raw.hdr workdir
[struct stat="OK", workspace="/path/to/workdir"]

.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.

.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
