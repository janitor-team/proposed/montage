.TH MPROJECTPP 1 "Dec 2016" "Montage 5" "Montage"
.SH NAME
mProjectPP \- Re-project FITS images

.SH SYNOPSIS
mProjectPP [\-z \fIfactor\fP] [\-d \fIlevel\fP] [\-b \fIborder\fP] [\-s \fIstatusfile\fP] [\-o \fIaltout.hdr\fP] [\-i \fIaltin.hdr\fP] [\-h \fIhdu\fP] [\-x \fIscale\fP] [\-w \fIweightfile\fP] [\-W \fIfixed-weight\fP] [\-t \fIthreshold\fP] [\-X] [\-b \fIborder-string\fP] in.fits out.fits template.hdr

.SH DESCRIPTION
\fBmProjectPP\fP reprojects a single image to the scale defined in an alternate FITS header template generated (usually) by \fBmTANhdr\fP. The program produces a pair of images: the reprojected image and an "area" image consisting of the fraction input pixel sky area that went into each output pixel.  This area image goes through all the subsequent processing that the reprojected image does, allowing it to be properly coadded at the end.
.PP
\fBmProjectPP\fP performs a plane-to-plane transform on the input image, and is an adaptation of the Mopex algorithm and developed in collaboration with the  Spitzer Space Telescope. It provides a speed increase of approximately a factor of 30 over the general-purpose \fBmProject\fP. However, \fBmProjectPP\fP is only suitable for projections which can be approximated by tangent-plane projections (TAN, SIN, ZEA, STG, ARC), and is therefore not suited for images covering large portions of the sky. Also note that it does not directly support changes in coordinate system (i.e. equatorial to galactic coordinates), though these changes can be facilitated by the use of an alternate header.

One situation that has happened often enough to warrant special note:  It is possible to define a FITS header with the
reference location off the image.  In particular, people often reference cylindrical projections (\fIe.g.,\fP CAR, CEA)
to location (0.,0.) (\fIe.g.,\fP the Galactic center).  This is not wrong in itself but does lead to extremely large
pixel offsets (\fIe.g.,\fP in the hundreds of thousands).  Related to this, if you extract a header from an image with
large offsets of this type, it is unlikely that you can simply change projection types without also adjusting the reference
location/offsets.  Most likely, you will end up with the reproject data all being off-scale.


.SH OPTIONS
.TP
\-z \fIfactor\fP
Processing is done utilizing the drizzle algorithm. \fIfactor\fP is a floating point number; recommended drizzle factors are from 0.5 to 1.
.TP
\-d \fIlevel\fP
Causes additional debugging information to be printed to stdout.  Valid levels are 1\-5; for levels greater than 1, it's recommended to redirect the output into a text file.
.TP
\-b \fIborder\fP
Ignores \fIborder\fP pixels around the image edge when performing calculations.
.TP
\-s \fIstatusfile\fP
Output and errors are written to \fIstatusfile\fP instead of being written to stdout.
.TP
\-[i|o] \fIalternate.hdr\fP
Specifies an alternate FITS header for use in \fBmProjectPP\fP calculations, allows substitution of psuedo-TAN headers created by mTANHdr.
.TP
\-h \fIhdu\fP
Specify the FITS extension to re-project if the FITS image is multi-extension.
.TP
\-x \fIscale\fP
Multiple the pixel values by \fIscale\fP when reprojecting.  For instance, each 2MASS image has a different scale factor (very near 1.0) to correct for varying magnitude-zero points.
.TP
\-w \fIweightfile\fP
Path to a weight map to be used when reading values from the input image.
.TP
\-W \fIfixed-weight\fP
Use constant weight value for the whole image.
.TP
\-t \fIthreshold\fP
Pixels with weights below \fIthreshold\fP will be treated as blank.
.TP
\-X
Makes the output region (originally defined in the header template) big enough to include all of the input images
.TP
\-b \fIborder-string\fP
Define a border width or border pixel corners outside of which all pixels are set to NaN.

.SH ARGUMENTS
.TP
in.fits
Input FITS file to be reprojected.
.TP
out.fits
Path to output FITS file to be created.
.TP
template.hdr
FITS header template to be used in generation of output FITS

.SH RESULT
Two files are created as output: the reprojected FITS file (\fIout.fits\fP), and an "area" image (\fIout_area.fits\fP). See the image reprojection algorithm for more information.

.SH MESSAGES
.TP
\fIOK\fP
[struct stat="\fIOK\fP", time=\fIseconds\fP]
.TP
ERROR
Drizzle factor string (\fIstring\fP) cannot be interpreted as a real number
.TP
ERROR
Cannot open status file: \fIstatusfile\fP
.TP
ERROR
Weight threshold string (\fIthreshold\fP) cannot be interpreted as a real number
.TP
ERROR
Flux scale string (\fIscale\fP) cannot be interpreted as a real number
.TP
ERROR
Border value string (\fIstring\fP) cannot be interprted as an integer or a set of polygon vertices
.TP
ERROR
Border value (\fIvalue\fP) must be greater than or equal to zero
.TP
ERROR
HDU value (\fIhdu\fP) must be a non-negative integer
.TP
ERROR
Could not set up plane-to-plane transform.  Check for compliant headers.
.TP
ERROR
No overlap
.TP
ERROR
Not enough memory for output data image array
.TP
ERROR
Not enough memory for output area image array
.TP
ERROR
Output wcsinit() failed.
.TP
ERROR
Input wcsinit() failed.
.TP
ERROR
Input and output must be in the same coordinate system for fast reprojection
.TP
ERROR
All pixels are blank
.TP
ERROR
Input image projection (\fIprojection\fP) must be TAN, SIN, ZEA, STG or ARC for fast reprojection
.TP
ERROR
Output image projection (\fIprojection\fP) must be TAN, SIN, ZEA, STG or ARC for fast reprojection
.TP
ERROR
Template file [\fItemplate.hdr\fP] not found
.TP
ERROR
Image file \fIin.fits\fP is missing or invalid FITS
.TP
ERROR
Weight file \fIweightfile\fP is missing or invalid FITS
.TP
ERROR
FITS library error

.SH EXAMPLES
.TP
$ mProjectPP rawdir/real_orig.fits projdir/base_unity.fits templates/galactic_orig.txt
[struct stat="OK", time=14]


.SH BUGS
The drizzle algorithm has been implemented but has not been tested
in this release.
.PP
If a header template contains carriage returns (i.e., created/modified
on a Windows machine), the cfitsio library will be unable to read it
properly, resulting in the error: [struct stat="ERROR", status=207,
msg="illegal character in keyword"]
.PP
It is best for the background correction algorithms if the area
described in the header template completely encloses all of the input
images in their entirety. If parts of input images are "chopped off"
by the header template, the background correction will be affected. We
recommend you use an expanded header for the reprojection and
background modeling steps, returning to the originally desired header
size for the final coaddition. The default background matching assumes
that there are no non-linear background variations in the individual
images (and therefore in the overlap differences). If there is any
uncertainty in this regard, it is safer to turn on the "level only"
background matching (the "\-l" flag in mBgModel.
.PP
mProjectPP is only suitable for use on projections which can be
approximated by tangent-plane projections (TAN, SIN, ZEA, STG, ARC),
and is therefore not suited for images covering large portions of the
sky. Also note that it does not directly support changes in coordinate
system (i.e. equatorial to galactic coordinates), though these changes
can be facilitated by the use of an alternate header.
.SH COPYRIGHT
2001-2015 California Institute of Technology, Pasadena, California
.PP
If your research uses Montage, please include the following
acknowledgement: "This research made use of Montage. It is funded by
the National Science Foundation under Grant Number ACI-1440620, and
was previously funded by the National Aeronautics and Space
Administration's Earth Science Technology Office, Computation
Technologies Project, under Cooperative Agreement Number NCC5-626
between NASA and the California Institute of Technology."
.PP
The Montage distribution includes an adaptation of the MOPEX algorithm
developed at the Spitzer Science Center.
